'use strict';
document.addEventListener('DOMContentLoaded', function(event) {
 
(function formValidation(){

    let form = document.querySelector(".form");
    let name = document.getElementById("name");
    let nameError = document.querySelector(".name-error")
    let email = document.getElementById("email");
    let emailError = document.querySelector(".email-error");
    let message = document.getElementById("message");
    let messageError = document.querySelector(".message-error");

    lengthControl(name, 4, 70);

    // email.addEventListener("input", function(event){

    //     if(email.validity.valid){
    //         error.innerHTML = ""; // Reset the content of the message
    //         error.className = "error"; // Reset the visual state of the message
    //     }

    // }, false);

    // form.addEventListener("submit", function (event) {
    //     // Each time the user tries to send the data, we check
    //     // if the email field is valid.
    //     if (!email.validity.valid) {
    //         console.log('invalid');
          
    //       // If the field is not valid, we display a custom
    //       // error message.
    //       error.innerHTML = "Please type your email";
    //       error.className = "error active t1";
    //       // And we prevent the form from being sent by canceling the event
    //       event.preventDefault();
    //     }
    //   }, false);



})();//end form validation

function lengthControl(element, minLength, maxLength){

    element.addEventListener("input", function(event) {

        let elementValue = element.value;

        if(elementValue.length < minLength){
            console.log("Use 4 characters at least.");
        }else if(elementValue.length >= maxLength){
            console.log("You have used the character limit.");
        }

    },false);

}


});//end document ready